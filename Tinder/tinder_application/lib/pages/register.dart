import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);
 
  @override
  State<RegisterScreen> createState() => _RegisterScreen();
}
 
class _RegisterScreen extends State<RegisterScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController repasswordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        
        body: Center(
        child: ListView(
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: SvgPicture.asset(
                  "assets/images/tinder_logo.svg"
                ),
            ),
                  
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Register',
                  style: TextStyle(fontSize: 20,color: Colors.pinkAccent),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: nameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'User Name',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
            ),
             Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                controller: repasswordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Confirm-Password',
                ),
              ),
            ),
            
            Container(
                height: 50,
                padding: const EdgeInsets.all(10),
                child: ElevatedButton(
                  child: const Text(
                    'Register',
                   style: TextStyle(fontSize: 20,backgroundColor: Colors.pinkAccent),
                  ),
                  
                  onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                title: const Text('Đăng kí'),
                content: const Text('Đăng kí thành công'),
                actions: <Widget>[
                TextButton(
                onPressed: () {
                 Navigator.pop(context);
                },
                child: const Text('Cancel'),
            ),
            

          ],
        ),
      ),
    
                )
            ),
            Container(
               
                height: 50,
                padding: const EdgeInsets.all(10),
                color: Colors.pinkAccent,
                child: ElevatedButton(
                
                  onPressed: () {
                   Navigator.pop(context);
                  },
                 child: const Text(
                   'Back to Login',
                   style: TextStyle(fontSize: 20),
                   ),
                )
            ),
            
          ],
        )),
    );
  }
}