

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tinder_application/pages/account_page.dart';
import 'package:tinder_application/pages/chat_page.dart';
import 'package:tinder_application/pages/explore_page.dart';
import 'package:tinder_application/pages/likes_page.dart';
class RootPagee extends StatelessWidget {
  RootPagee({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     
    );
  }
}


class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage>{
  int _selectedIndex = 0;
  final screens = [
    ExplorePage(),
    LikesPage(),
    ChatPage(),
    AccountPage(),
  ];
   void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: new IconButton(
          onPressed: (() {
            Navigator.push(
                         context, 
                      MaterialPageRoute(builder: (context) => AccountPage()
                       ));
        }), icon: Icon(Icons.account_circle),color: Colors.black,),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            color: Colors.pinkAccent,
            onPressed:(() {}),
          ),
          ]
          ),
          body: screens[_selectedIndex],
          bottomNavigationBar: BottomNavigationBar(
            
          backgroundColor: Colors.white,
          items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
             
              child: SvgPicture.asset(
               
                //"assets/images/explore_active_icon.svg",
                "assets/images/explore_icon.svg",
              ),
              
            ),
            activeIcon: SvgPicture.asset("assets/images/explore_active_icon.svg"),
            label:"",
           
          ),
          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
              
              child: SvgPicture.asset(
                "assets/images/likes_icon.svg",
              ),
              ),
              activeIcon: SvgPicture.asset("assets/images/likes_active_icon.svg"),
            label:"",
            
          ),
          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
              child: SvgPicture.asset(
                "assets/images/chat_icon.svg",
              ),
              
              ),
              activeIcon: SvgPicture.asset("assets/images/chat_active_icon.svg"),
            
            label:"",
          ),
          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
              child: SvgPicture.asset(
              "assets/images/account_icon.svg"),
              ),
              activeIcon: SvgPicture.asset("assets/images/account_active_icon.svg"),
            label:"",
        
          ),
        ],
        currentIndex: _selectedIndex,
        
        onTap: _onItemTapped,
        ),
  );
  }
}
  
  
  
    
  


