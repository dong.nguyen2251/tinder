import 'package:flutter/material.dart';
import 'package:tinder_application/pages/register.dart';
import 'package:tinder_application/pages/root_app.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: RegisterScreen(),
  ));
}

